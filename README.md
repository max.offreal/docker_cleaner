To read logs:

```
journalctl --user | grep "RUNNER FREE SPACE"
```
... if runned as current user, or
```
journalctl --system | grep "RUNNER FREE SPACE"

```
... if runned as root.

Cron job: `crontab -e` or `sudo crontab -e`

```
0 5 * * * /home/offreal/docker_cleaner/clean.sh

```

means every day at 05:00
