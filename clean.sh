#!/bin/bash
set -euo pipefail

MOUNTPOINT="/var/lib/docker/"
MIN_LEFT_SPACE=10000000
# 10 GB -- 10 000 000 KB blocks

function xecho () {
        echo "$@"
        logger -t "RUNNER FREE SPACE WGD" "$@"
}

function space_ok {
        xecho "All ok, there are nothing to clean"
}

function docker_clean {
        xecho "docker remove dangling images ..."
        docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
        xecho "docker clear builder cache ..."
        docker builder prune -f
        xecho "DONE !!!"
}

xecho "Checking $MOUNTPOINT:"

left_space=$(df -k $MOUNTPOINT | awk '/[0-9]%/{print $(NF-2)}')

xecho "Left on disk: $left_space"
xecho "Min Left:     $MIN_LEFT_SPACE"

if (( left_space < MIN_LEFT_SPACE )); then
        docker_clean
else
        space_ok
fi
